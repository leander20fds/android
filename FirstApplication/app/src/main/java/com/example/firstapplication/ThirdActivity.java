package com.example.firstapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import static android.widget.Toast.LENGTH_SHORT;

public class ThirdActivity extends AppCompatActivity {
    private TextView time;
    private TextView nome;
    private TextView idade;
    private Button botao;
    private String[] times = { "Internacional",
                               "Flamengo",
                               "Palmeiras",
                               "Gremio" };

    private String[] nomes = { "Leander",
                               "Deivid",
                               "Fernanda",
                               "Carlos" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onCreate()", LENGTH_SHORT);
        toast.show();

        time = (TextView) findViewById(R.id.time);
        nome = (TextView) findViewById(R.id.nome);
        idade = (TextView) findViewById(R.id.idade);
        botao = (Button) findViewById(R.id.botao);

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random randomico = new Random();

                int indexTime = randomico.nextInt(times.length);
                int indexNome = randomico.nextInt(nomes.length);
                int numeroAleatorio = randomico.nextInt(36);

                time.setText(times[indexTime]);
                nome.setText(nomes[indexNome]);
                idade.setText(Integer.toString(numeroAleatorio));
            }
        });

        Button btn = (Button)findViewById(R.id.prev);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ThirdActivity.this, SecondaryActivity.class));
            }
        });
    }

    protected void onStart(){
        super.onStart();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "OnStart()", LENGTH_SHORT);
        toast.show();
    }
    protected void onResume(){
        super.onResume();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onResume()", LENGTH_SHORT);
        toast.show();
    }
    protected void onPause(){
        super.onPause();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onPause()", LENGTH_SHORT);
        toast.show();
    }
    protected void onRestart(){
        super.onRestart();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onRestart()", LENGTH_SHORT);
        toast.show();
    }
    protected void onStop(){
        super.onStop();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onStop()", LENGTH_SHORT);
        toast.show();
    }
    protected void onDestroy(){
        super.onDestroy();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onDestroy()", LENGTH_SHORT);
        toast.show();
    }
}
