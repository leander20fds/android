package com.example.firstapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import static android.widget.Toast.LENGTH_SHORT;

public class SecondaryActivity extends AppCompatActivity {
    private TextView frase;
    private Button botao;
    private String[] frasesDoDia = { "Nada é tão ruim que não possa piorar.",
                                     "Pior que a morte só a vida.",
                                     "Em terra de cego quem tem olho não é cego.",
                                     "O Naruto pode ser duro as vezes." };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onCreate()", LENGTH_SHORT);
        toast.show();

        frase = (TextView) findViewById(R.id.frase);
        botao = (Button) findViewById(R.id.botao);

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random randomico = new Random();
                int numeroAleatorio = randomico.nextInt(frasesDoDia.length);

                frase.setText(frasesDoDia[numeroAleatorio]);
            }
        });

        Button btnPrev = (Button)findViewById(R.id.prev);
        Button btnNext = (Button)findViewById(R.id.next);

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SecondaryActivity.this, MainActivity.class));
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SecondaryActivity.this, ThirdActivity.class));
            }
        });
    }

    protected void onStart(){
        super.onStart();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "OnStart()", LENGTH_SHORT);
        toast.show();
    }
    protected void onResume(){
        super.onResume();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onResume()", LENGTH_SHORT);
        toast.show();
    }
    protected void onPause(){
        super.onPause();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onPause()", LENGTH_SHORT);
        toast.show();
    }
    protected void onRestart(){
        super.onRestart();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onRestart()", LENGTH_SHORT);
        toast.show();
    }
    protected void onStop(){
        super.onStop();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onStop()", LENGTH_SHORT);
        toast.show();
    }
    protected void onDestroy(){
        super.onDestroy();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onDestroy()", LENGTH_SHORT);
        toast.show();
    }
}
