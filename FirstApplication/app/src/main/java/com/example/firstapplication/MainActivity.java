package com.example.firstapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import static android.widget.Toast.LENGTH_SHORT;

public class MainActivity extends AppCompatActivity {
    private TextView numero;
    private Button advinha;
    private String[] valores = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intentTwo = getIntent();
        String dicas = intentTwo.getStringExtra("Dica");

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, dicas, LENGTH_SHORT);
        toast.show();

        numero = (TextView) findViewById(R.id.nro);
        advinha = (Button) findViewById(R.id.adv);

        advinha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random randomico = new Random();
                int numeroAleatorio = randomico.nextInt(valores.length);

                numero.setText(valores[numeroAleatorio]);
            }
        });

        Button btn = (Button)findViewById(R.id.next);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(MainActivity.this, SecondaryActivity.class));
            }
        });
    }

    protected void onStart(){
        super.onStart();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "OnStart()", LENGTH_SHORT);
        toast.show();
    }
    protected void onResume(){
        super.onResume();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onResume()", LENGTH_SHORT);
        toast.show();
    }
    protected void onPause(){
        super.onPause();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onPause()", LENGTH_SHORT);
        toast.show();
    }
    protected void onRestart(){
        super.onRestart();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onRestart()", LENGTH_SHORT);
        toast.show();
    }
    protected void onStop(){
        super.onStop();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onStop()", LENGTH_SHORT);
        toast.show();
    }
    protected void onDestroy(){
        super.onDestroy();

        Context contexto = getApplicationContext();
        Toast toast = Toast.makeText(contexto, "onDestroy()", LENGTH_SHORT);
        toast.show();
    }
}
